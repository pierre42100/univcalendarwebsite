# UnivCalendarWebsite

Website that present my calendar apps.

## Building
```
apt install -y npm jekyll
npm install -g gulp
gulp
jekyll serve -w
```

## Credit
This website is based on [New Age theme from StartBootstrap](https://startbootstrap.com/template-overviews/new-age/) and [Antonio Trento theme](https://github.com/jekynewage/jekynewage.github.io).